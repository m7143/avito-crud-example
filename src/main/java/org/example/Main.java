package org.example;

import lombok.extern.slf4j.Slf4j;
import org.example.data.Flat;
import org.example.manager.AvitoManager;
import org.example.request.SearchRequest;

import java.util.List;

@Slf4j
public class Main {
    public static void main(String[] args) {
        Flat flat1 = new Flat(0, "studio", 1000000, 30, true, false, 1, 9, false);
        Flat flat2 = new Flat(1, "1", 1200000, 40, false, true, 2, 9, false);
        Flat flat3 = new Flat(2, "2", 1400000, 50, true, false, 3, 20, false);
        Flat flat4 = new Flat(3, "3", 1800000, 60, true, false, 4, 5, false);
        Flat flat5 = new Flat(4, "4", 2200000, 70, false, false, 4, 5, false);
        Flat flat6 = new Flat(5, "5", 2600000, 80, true, false, 4, 20, false);
        Flat flat7 = new Flat(6, "6", 2900000, 90, true, false, 5, 5, false);
        Flat flat8 = new Flat(7, "free_style", 2000000, 50, true, false, 5, 5, false);

        SearchRequest search1 = new SearchRequest(new String[]{"2"}, 1000000, 5000000, 20, 100,
                true, false, 2, 5, 2, 19, false, false);

        SearchRequest search2 = new SearchRequest(new String[]{"2", "5"}, 1000000, 5000000, 20, 100,
                true, false, 2, 5, 2, 19, false, false);

        AvitoManager flatManager = new AvitoManager();
        flatManager.create(flat1);
        flatManager.create(flat2);
        flatManager.create(flat3);
        flatManager.create(flat4);
        flatManager.create(flat5);
        flatManager.create(flat6);
        flatManager.create(flat7);
        flatManager.create(flat8);

        List<Flat> result1 = flatManager.searchBy(search1);
        log.debug("searchBy, result1: {}", result1 + "\n");

        List<Flat> result2 = flatManager.searchBy(search2);
        log.debug("searchBy, result2: {}", result2 + "\n");

        int count = flatManager.getCount();
        log.debug("getCount, count: {}", count + "\n");

    }
}
